package nl.tudelft.spectata.api.accessor;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import nl.tudelft.spectata.api.domain.Data;

import java.util.List;
import java.util.UUID;

@Accessor
public interface DataAccessor {
    @Query("SELECT * FROM mykeyspace.Data where buildid IN ?")
    Result<Data> get(List<UUID> buildId);

    @Query("SELECT * FROM mykeyspace.Data where buildid = ? and dataid = ?")
    Result<Data> get(UUID buildId, UUID dataId);
}
