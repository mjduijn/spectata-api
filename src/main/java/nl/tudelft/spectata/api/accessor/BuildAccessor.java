package nl.tudelft.spectata.api.accessor;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import nl.tudelft.spectata.api.domain.Build;

@Accessor
public interface BuildAccessor {
    @Query("SELECT * FROM mykeyspace.Build")
    Result<Build> getAll();
}
