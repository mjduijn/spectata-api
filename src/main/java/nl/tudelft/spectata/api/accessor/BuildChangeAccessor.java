package nl.tudelft.spectata.api.accessor;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import nl.tudelft.spectata.api.domain.BuildChange;

@Accessor
public interface BuildChangeAccessor {
    @Query("SELECT * FROM mykeyspace.BuildChange;")
    Result<BuildChange> getAll();
}
