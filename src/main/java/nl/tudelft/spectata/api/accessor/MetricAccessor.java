package nl.tudelft.spectata.api.accessor;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import nl.tudelft.spectata.api.domain.Metric;

import java.util.List;
import java.util.UUID;

@Accessor
public interface MetricAccessor {
    @Query("SELECT * FROM mykeyspace.Metric where buildid IN ?")
    Result<Metric> get(List<UUID> buildId);

    @Query("SELECT * FROM mykeyspace.Metric where buildid = ? and path = ?")
    Result<Metric> get(UUID buildId, String path);

    @Query("SELECT * FROM mykeyspace.Metric where metric = ?")
    Result<Metric> getOnMetric(String metric);
}
