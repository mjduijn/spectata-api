package nl.tudelft.spectata.api.accessor;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import nl.tudelft.spectata.api.domain.ProjectScore;

import java.util.UUID;

@Accessor
public interface ProjectScoreAccessor {
    @Query("SELECT * FROM mykeyspace.ProjectScore")
    Result<ProjectScore> get();
    @Query("SELECT * FROM mykeyspace.ProjectScore where buildid = ?")
    Result<ProjectScore> get(UUID buildId);
    @Query("SELECT * FROM mykeyspace.ProjectScore where projectkey = ?")
    Result<ProjectScore> get(String projectKey);
    @Query("SELECT * FROM mykeyspace.ProjectScore where buildid = ? and projectkey = ?")
    Result<ProjectScore> get(UUID buildId, String projectKey);
}
