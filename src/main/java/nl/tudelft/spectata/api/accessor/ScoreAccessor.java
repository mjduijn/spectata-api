package nl.tudelft.spectata.api.accessor;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import nl.tudelft.spectata.api.domain.ProjectScore;
import nl.tudelft.spectata.api.domain.Score;

import java.util.List;
import java.util.UUID;

@Accessor
public interface ScoreAccessor {
    @Query("SELECT * FROM mykeyspace.Score where buildid IN ?")
    Result<Score> get(List<UUID> buildId);
}
