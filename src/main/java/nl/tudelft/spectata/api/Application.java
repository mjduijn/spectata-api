package nl.tudelft.spectata.api;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static final String keyspace = "mykeyspace";
    public static Cluster cluster = null;
    public static Session session = null;
    public static MappingManager manager = null;

    public static void main(String[] args) {
        //First argument is cluster address
        String clusterHost = args[0];

        try {
            cluster = Cluster.builder()
                    .addContactPoint(clusterHost)
                    .build();
            session = cluster.connect();

            ResultSet rs = session.execute("select release_version from system.local");
            Row row = rs.one();
            System.out.println(row.getString("release_version"));
        } catch(Exception e){
            e.printStackTrace();
            System.exit(1);
        }
        manager = new MappingManager(session);

        SpringApplication.run(Application.class, args);
    }
}