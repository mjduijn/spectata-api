package nl.tudelft.spectata.api.controller;

import com.datastax.driver.mapping.MappingManager;
import lombok.extern.slf4j.Slf4j;
import nl.tudelft.spectata.api.Application;
import nl.tudelft.spectata.api.accessor.*;
import nl.tudelft.spectata.api.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping(value={"/api"})
public class ExternalController {

    private MappingManager manager = Application.manager;
    private ProjectScoreAccessor projectScoreAccessor = manager.createAccessor(ProjectScoreAccessor.class);
    private ScoreAccessor scoreAccessor = manager.createAccessor(ScoreAccessor.class);
    private MetricAccessor metricAccessor = manager.createAccessor(MetricAccessor.class);
    private DataAccessor dataAccessor = manager.createAccessor(DataAccessor.class);

    @RequestMapping(value = {"/score/build"}, produces = "application/json")
    public ResponseEntity<List<ProjectScore>> getProjectScores(@RequestParam(required = false) final Optional<String> project_id,
                                              @RequestParam(required = false) final Optional<UUID> build_id) {

        if(build_id.isPresent() && project_id.isPresent()) {
            return new ResponseEntity<>(projectScoreAccessor.get(build_id.get(), project_id.get()).all(), HttpStatus.OK);
        }
        else if(build_id.isPresent()) {
            return new ResponseEntity<>(projectScoreAccessor.get(build_id.get()).all(), HttpStatus.OK);
        }
        else if(project_id.isPresent()) {
            return new ResponseEntity<>(projectScoreAccessor.get(project_id.get()).all(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(projectScoreAccessor.get().all(), HttpStatus.OK);
        }
    }

    @RequestMapping(value = {"/score/file"}, produces = "application/json")
    public ResponseEntity<List<Score>> getScores(@RequestParam final List<UUID> build_ids,
                                                 @RequestParam(required = false) final Optional<String> path) {
        List<Score> scores = scoreAccessor.get(build_ids).all();
        if(path.isPresent()) {
            scores = scores.stream().filter(s -> s.getProductionPath().equals(path.get())
                    || s.getTestPath().equals(path.get())).collect(Collectors.toList());
        }
        scores.sort((s1, s2) ->  s1.getValue().compareTo(s2.getValue()));
        return new ResponseEntity<>(scores, HttpStatus.OK);
    }
    @RequestMapping(value = {"/metric"}, produces = "application/json")
    public ResponseEntity<List<Metric>> getMetrics(@RequestParam final List<UUID> build_ids,
                                                 @RequestParam(required = false) final Optional<String> path) {

        List<Metric> metrics = metricAccessor.get(build_ids).all();
        if(path.isPresent()) {
            metrics = metrics.stream().filter(m -> m.getPath().equals(path.get())).collect(Collectors.toList());
        }
        return new ResponseEntity<>(metrics, HttpStatus.OK);
    }

    @RequestMapping(value = {"/item"}, produces = "application/json")
    public ResponseEntity<List<Data>> getIssues(@RequestParam final List<UUID> build_ids,
                                                @RequestParam(required = false) final Optional<String> path,
                                                @RequestParam(required = false) final Optional<String> metric) {

        List<Data> data = dataAccessor.get(build_ids).all();
        if(metric.isPresent()) {
            List<UUID> dataIds = metricAccessor.getOnMetric(metric.get()).all().stream()
                    .flatMap(m -> m.getReferences().stream())
                    .distinct()
                    .collect(Collectors.toList());

            data = data.stream().filter(d -> dataIds.contains(d.getDataId())).collect(Collectors.toList());
        }
        if(path.isPresent()) {
            data = data.stream().filter(d -> d.getRefs().stream().anyMatch(c -> c.getPath().equals(path)))
                    .collect(Collectors.toList());
        }
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @RequestMapping(value = {"/compare/metric"}, produces = "application/json")
    public ResponseEntity<List<MetricChange>> compareProject(@RequestParam final UUID start,
                                                             @RequestParam final UUID end,
                                                             @RequestParam final String path) {

        List<Metric> oldMetrics = metricAccessor.get(start, path).all();
        List<Metric> newMetrics = metricAccessor.get(end, path).all();
        if(oldMetrics.size() == 0 || newMetrics.size() == 0) {
            throw new NoSuchElementException("Build ID or path not found");
        }

        //"left outer join" with old metrics
        Map<String, MetricChange> changes = newMetrics.stream().collect(Collectors.toMap(Metric::getMetric, m -> new MetricChange(m)));
        oldMetrics.forEach(m -> {
            if(changes.containsKey(m.getMetric())) {
                MetricChange change = changes.get(m.getMetric());
                change.setOldMetric(m);
                changes.put(m.getMetric(), change);
            }
        });

        //Set new and old issues
        List<MetricChange> l = new ArrayList<>(changes.values());
        l.forEach(mc -> mc.getNewMetric().getReferences().forEach(id -> {
            Data d = dataAccessor.get(end, id).one();
            if(mc.getOldMetric().getReferences().contains(id)) {
                mc.addUnchangedIssue(d);
            } else {
                mc.addNewIssue(d);
            }
        }));
        return new ResponseEntity<>(l, HttpStatus.OK);
    }

    @RequestMapping(value = {"/compare/file"}, produces = "application/json")
    public ResponseEntity<List<ScoreChange>> compareProject(@RequestParam final UUID start,
                                                            @RequestParam final UUID end) {

        List<Score> oldScores = scoreAccessor.get(Collections.singletonList(start)).all();
        List<Score> newScores = scoreAccessor.get(Collections.singletonList(end)).all();
        if(oldScores.size() == 0 || newScores.size() == 0) {
            throw new NoSuchElementException("Build ID not found");
        }

        //"left outer join" with old scores
        Map<String, ScoreChange> changes = newScores.stream().collect(Collectors.toMap(s -> s.getTestPath() + s.getAspect(), s -> new ScoreChange(s)));
        oldScores.forEach(s -> {
            if(changes.containsKey(s.getTestPath() + s.getAspect())) {
                ScoreChange change = changes.get(s.getTestPath() + s.getAspect());
                change.setOldScore(s);
                changes.put(s.getTestPath() + s.getAspect(), change);
            }
        });

        List<ScoreChange> l = new ArrayList<>(changes.values());
        l.sort((sc1, sc2) -> ((Double) (sc1.getNewScore().getValue() - sc1.getOldScore().getValue())).compareTo(sc2.getNewScore().getValue() - sc2.getOldScore().getValue()));
        return new ResponseEntity<>(l, HttpStatus.OK);
    }
}
