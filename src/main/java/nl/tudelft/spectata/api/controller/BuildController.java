package nl.tudelft.spectata.api.controller;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import lombok.extern.slf4j.Slf4j;
import nl.tudelft.spectata.api.Application;
import nl.tudelft.spectata.api.accessor.BuildAccessor;
import nl.tudelft.spectata.api.accessor.BuildChangeAccessor;
import nl.tudelft.spectata.api.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import static com.datastax.driver.mapping.Mapper.Option.saveNullFields;

@Slf4j
@RestController
@RequestMapping(value={"/api/build"})
public class BuildController {

    private MappingManager manager = Application.manager;
    private BuildAccessor buildAccessor = manager.createAccessor(BuildAccessor.class);
    private BuildChangeAccessor buildChangeAccessor = manager.createAccessor(BuildChangeAccessor.class);

    @RequestMapping(produces = "application/json")
    public ResponseEntity<List<Build>> getAll() {
        return new ResponseEntity<>(buildAccessor.getAll().all(), HttpStatus.OK);
    }
    @RequestMapping(produces = "application/json", method = {RequestMethod.POST}, consumes = "application/json")
    public ResponseEntity<Build> setBuild(@RequestBody Build build) {
        manager.mapper(Build.class).save(build, saveNullFields(false));
        return new ResponseEntity<>(build, HttpStatus.OK);
    }

    @RequestMapping(value = {"/issue", "/issues"}, method = {RequestMethod.POST}, produces = "application/json", consumes = "application/json")
    ResponseEntity<List<BuildData>> setIssue(@RequestBody List<BuildIssue> buildIssues) {
        List<BuildData> l = new LinkedList<BuildData>();
        for (BuildIssue buildIssue: buildIssues) {
            if(buildIssue.getIssueId() == null) {
                buildIssue.setIssueId(UUID.randomUUID());
            }
            if(!buildIssue.getKey().startsWith("src")) {
                String[] ba = buildIssue.getKey().split(":");
                buildIssue.setKey(ba[ba.length - 1]);
            }
            if(!buildIssue.getPath().startsWith("src")) {
                String[] ba = buildIssue.getPath().split(":");
                buildIssue.setPath(ba[ba.length - 1]);
            }
            l.add(new BuildData(buildIssue.getBuildId(), buildIssue.getIssueId(), buildIssue.getRule(),
                    buildIssue.getValue(), Arrays.asList(new CodeRef(buildIssue.getPath(), buildIssue.getKey(),
                    buildIssue.getLineNumbers()))));
        }
        return this.setData(l);
    }

    @RequestMapping(value = "/change", produces = "application/json")
    public ResponseEntity<List<BuildChange>> getBuildChange() {
        return new ResponseEntity<>(buildChangeAccessor.getAll().all(), HttpStatus.OK);
    }
    @RequestMapping(value = "/change", produces = "application/json", method = {RequestMethod.POST}, consumes = "application/json")
    public ResponseEntity<BuildChange> setBuildChange(@RequestBody BuildChange b) {
        manager.mapper(BuildChange.class).save(b);
        return new ResponseEntity<>(b, HttpStatus.OK);
    }

    @RequestMapping(value = "/data", produces = "application/json", method = {RequestMethod.POST}, consumes = "application/json")
    public ResponseEntity<List<BuildData>> setData(@RequestBody List<BuildData> bd) {
        Mapper buildDataMapper = manager.mapper(BuildData.class);
        for (BuildData data: bd) {
            if(data.getDataId() == null) {
                data.setDataId(UUID.randomUUID());
            }
            buildDataMapper.save(data);
        }
        return new ResponseEntity<>(bd, HttpStatus.OK);
    }

    @RequestMapping(value = "/testcoupling", produces = "application/json", method = {RequestMethod.POST}, consumes = "application/json")
    public ResponseEntity<List<TestCoupling>> setTestCoupling(@RequestBody List<TestCoupling> bd) {
        Mapper buildDataMapper = manager.mapper(TestCoupling.class);
        bd.forEach(buildDataMapper::save);
        return new ResponseEntity<>(bd, HttpStatus.OK);
    }

    @RequestMapping(value = "/dependantchange", produces = "application/json", method = {RequestMethod.POST}, consumes = "application/json")
    public ResponseEntity<List<DependantChange>> setDependantChange(@RequestBody List<DependantChange> bd) {
        Mapper m = manager.mapper(DependantChange.class);
        bd.forEach(m::save);
        return new ResponseEntity<>(bd, HttpStatus.OK);
    }
}
