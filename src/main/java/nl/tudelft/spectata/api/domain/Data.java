package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.Frozen;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import nl.tudelft.spectata.api.Application;

import java.util.List;
import java.util.UUID;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@Table(keyspace = Application.keyspace, name = "Data")
public class Data {
    @PartitionKey(value = 0)
    UUID buildId;
    @PartitionKey(value = 1)
    UUID dataId;
    String rule;
    String value;
    @Frozen
    List<CodeRef> refs;
}