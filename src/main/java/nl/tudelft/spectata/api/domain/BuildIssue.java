package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class BuildIssue {
    @PartitionKey(value = 0)
    UUID buildId;
    @PartitionKey(value = 1)
    UUID issueId;
    String rule;
    String path;
    String key;
    String value;
    List<Integer> lineNumbers;
}