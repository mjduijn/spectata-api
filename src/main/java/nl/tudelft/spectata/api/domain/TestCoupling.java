package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.spectata.api.Application;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(keyspace = Application.keyspace, name = "BuildTestCoupling")
public class TestCoupling {
    @PartitionKey(value = 0)
    UUID buildId;
    @PartitionKey(value = 1)
    String testKey;
    @PartitionKey(value = 2)
    String productionKey;
    @PartitionKey(value = 3)
    Integer line;
}