package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.UDT;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.spectata.api.Application;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@UDT(keyspace = Application.keyspace, name = "CodeRef")
public class CodeRef {
    String path;
    String key;
    List<Integer> lineNumbers;
}
