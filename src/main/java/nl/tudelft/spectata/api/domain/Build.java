package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.spectata.api.Application;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Table(keyspace = Application.keyspace, name = "Build")
@Data
@NoArgsConstructor
public class Build {
    @PartitionKey
    UUID buildId;

    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ssXXX", timezone="CET")
    Date startTs;
    String projectKey;
    String gitBranch;
    String jenkinsBuildTag;
    List<String> scmRevisions;
    UUID parentBuildId;
    String status;

    public Build(UUID buildId) {
        this.buildId = buildId;
    }
}