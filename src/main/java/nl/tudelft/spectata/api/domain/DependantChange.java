package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.spectata.api.Application;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Table(keyspace = Application.keyspace, name = "DependantChange")
@Data
@NoArgsConstructor
public class DependantChange {
    @PartitionKey
    UUID buildId;
    String revision;
    String path;
    String change;
    List<Integer> lineNumbersAdded;
    List<Integer> lineNumbersRemoved;
}