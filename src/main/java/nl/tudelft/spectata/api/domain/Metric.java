package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import nl.tudelft.spectata.api.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Table(keyspace = Application.keyspace, name = "Metric")
@Data
@NoArgsConstructor
public class Metric {
    @PartitionKey(value = 0)
    UUID buildId;
    @PartitionKey(value = 1)
    String path;
    @PartitionKey(value = 2)
    String metric;
    Double value;
    Double normalizedValue;
    List<UUID> references = new ArrayList<>();
}