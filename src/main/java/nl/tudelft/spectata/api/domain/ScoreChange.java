package nl.tudelft.spectata.api.domain;

import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
public class ScoreChange {
    Score oldScore;
    Score newScore;

    public ScoreChange(Score newScore) {
        this.newScore = newScore;
    }
}
