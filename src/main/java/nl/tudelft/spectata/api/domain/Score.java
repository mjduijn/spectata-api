package nl.tudelft.spectata.api.domain;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.spectata.api.Application;

import java.util.List;
import java.util.UUID;

@Table(keyspace = Application.keyspace, name = "Score")
@Data
@NoArgsConstructor
public class Score {
    @PartitionKey(value = 0)
    UUID buildId;
    @PartitionKey(value = 1)
    String aspect;
    @PartitionKey(value = 2)
    String testPath;
    String productionPath;
    Double value;
    List<String> references;
}