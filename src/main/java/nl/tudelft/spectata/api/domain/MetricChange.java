package nl.tudelft.spectata.api.domain;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@lombok.Data
@NoArgsConstructor
public class MetricChange {
    Metric oldMetric;
    Metric newMetric;
    List<Data> unchangedIssues = new ArrayList<>();
    List<Data> newIssues = new ArrayList<>();

    public MetricChange(Metric newMetric) {
        this.newMetric = newMetric;
    }

    public void addUnchangedIssue(Data d) {
        unchangedIssues.add(d);
    }
    public void addNewIssue(Data d) {
        newIssues.add(d);
    }
}
