#!/usr/bin/env bash

export SPRING_CONFIG_LOCATION=/application.properties

if [ "$RACK_ENV" = 'development' ]; then
    cp /scripts/dev.application.properties $SPRING_CONFIG_LOCATION
    cluster='storage-dummy'
    cluster='spectata-storage'
fi

cat $SPRING_CONFIG_LOCATION
jar=$(find target -name "*exec*" | head -1)
java -jar $jar $cluster