FROM base-java
MAINTAINER maarten.duijn@ing.nl

# What to run when the container starts
CMD [ "/scripts/run.sh" ]
EXPOSE 8080

COPY scripts /scripts
RUN chmod +x /scripts -R

COPY "target/cassandra-api-*-exec.jar" "/target/"