#!/usr/bin/env bash

#Build project
wd=$(pwd)
cd ..
mvn package

#Build Jar image
cd $wd
jar=$(find ../target/ -name "cassandra-api*-SNAPSHOT.jar" | head -1)
cp $jar cassandra-api.jar
echo "Running with: $jar"

sudo docker-compose up