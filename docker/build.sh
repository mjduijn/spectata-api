#!/usr/bin/env bash

wd=$(pwd)

#Build project
cd ..
mvn clean package

#Build docker image
cd $wd
tag="utqm/cassandra-api"
jar=$(find ../target/ -name "cassandra-api*" | head -1)
echo "Building with: $jar"
cp $jar cassandra-api.jar
sudo docker build --build-arg http_proxy=$http_proxy --build-arg https_proxy=$https_proxy --build-arg no_proxy=$no_proxy -t=$tag .

echo "Built: $tag"
